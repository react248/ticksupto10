import { Component } from "react";

export class Counter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ticks: 0,
      disabled: false,
    };
  }

  incrementToTen = () => {
    this.setState({ ticks: this.state.ticks + 1 });
  };

  render() {
    return (
      <button disabled={this.state.ticks === 10} onClick={this.incrementToTen}>
        {this.state.ticks}
      </button>
    );
  }
}
